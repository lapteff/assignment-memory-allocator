//
// Created by maks- on 12.01.2023.
//

#include "tests.h"
#include "mem.h"
#include "util.h"

#define FIRST_SIZE 100
#define SECOND_SIZE 300
#define BIG_SIZE 64000

bool test_alloc_mem(){
    void* first_block = _malloc(FIRST_SIZE);
    void* second_block = _malloc(SECOND_SIZE);

    bool result = false;
    if (first_block && second_block){
        if (block_get_header(first_block)->capacity.bytes==FIRST_SIZE && block_get_header(second_block)->capacity.bytes==SECOND_SIZE){
            result = true;
        }
    }

    _free(first_block);
    _free(second_block);
    return result;
}

bool test_free_one_block(){
    void* block = _malloc(FIRST_SIZE);
    _free(block);

    return block_get_header(block)->is_free;
}

bool test_free_two_blocks(){
    void* block1 = _malloc(FIRST_SIZE);
    void* block2 = _malloc(SECOND_SIZE);
    _free(block1);
    _free(block2);

    return block_get_header(block1)->is_free && block_get_header(block2)->is_free;
}

bool test_grow_heap(){
    void* block = _malloc(BIG_SIZE);
    struct block_header* hdr = block_get_header(block);

    if (!hdr->is_free && hdr->capacity.bytes == BIG_SIZE){
        _free(block);
        return true;
    }
    _free(block);
    return false;
}

bool test_grow_heap_in_another_place(){
    bool result = false;
    void* block1 = _malloc(16000);
    void* block2 = _malloc(BIG_SIZE);
    if (block_get_header(block1)->capacity.bytes == 16000 && block_get_header(block2)->capacity.bytes == BIG_SIZE
    && !block_get_header(block1)->is_free && !block_get_header(block2)->is_free){
        result = true;
    }
    _free(block1);
    _free(block2);

    return result;
}


void run_tests(){
    int8_t result = 0;
    if (test_alloc_mem()){
        result ++;
        printf("TEST 1 PASSED\n");
    } else {
        printf("TEST 1 FAILED. Memory wasn't allocated\n");
    }

    if (test_free_one_block()){
        result ++;
        printf("TEST 2 PASSED\n");
    } else {
        printf("TEST 2 FAILED. Failed freeing one block\n");
    }

    if (test_free_two_blocks()){
        result ++;
        printf("TEST 3 PASSED\n");
    } else {
        printf("TEST 3 FAILED. Failed freeing two blocks\n");
    }

    if (test_grow_heap()){
        result ++;
        printf("TEST 4 PASSED\n");
    } else {
        printf("TEST 4 FAILED. New region didn't expand the old one\n");
    }

    if (test_grow_heap_in_another_place()){
        result ++;
        printf("TEST 5 PASSED\n");
    } else {
        printf("TEST 5 FAILED. New block wasn't allocated at new place\n");
    }

    fprintf(stdout, "\nRESULT: %d passed, %d failed\n", result, 5 - result);
}